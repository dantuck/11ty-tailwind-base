module.exports = {
  purge: {
    content: [
      "src/**/*.njk",
      "src/**/*.md"
    ],
    options: {
      whitelist: [],
    },
  },
  theme: {},
  variants: {},
  plugins: [],
};