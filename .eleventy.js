module.exports = function(eleventyConfig) {
  eleventyConfig.setUseGitIgnore(false);

  // base.njk aliased to base for easier access
  eleventyConfig.addLayoutAlias('base', 'layouts/base.njk');

  // Watch for CSS changes
  eleventyConfig.addWatchTarget("./.build/css/style.css");
  // Copy CSS build changes to dist css/style.css
  eleventyConfig.addPassthroughCopy({ "./.build/css/style.css": "css/style.css" });
  
  return  {
    dir: {
      // Where to look for our site
      input: "src/site",
      includes: "_includes",
      // Where to place our generated site
      output: "dist"
    },
    passthroughFileCopy: true,
    templateFormats : ["njk", "md"],
    htmlTemplateEngine : "njk",
    markdownTemplateEngine : "njk",
  };
};